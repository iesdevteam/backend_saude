# BACKEND SAUDE-IES

## PASSOS PARA RODAR O PROJETO

- Instalar o PostgreSQL
    * o projeto está configurado para
        * 'NAME': 'saide_ies'
        * 'HOST': 'localhost'
        * 'PORT': '5432'
        * 'USER': 'postgres'
        * 'PASSWORD': 'root'
        * ### Qualquer dado diferente destes, mudar em DATABASES no arquivo settings.py
        
##### O projeto usa o VirtualEnv - que cria um "python virtual" independente do criado na sua maquina - isso facilita a instalação das bilbiotecas
###### para instalar:
* entrar na pasta raiz do projeto, e rodar os seguintes comandos:
    - ```virtualenv venv```
    - ```venv/Scripts/activate```
    - ```pip install -r requirements.txt```
     
