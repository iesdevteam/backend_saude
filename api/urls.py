from django.urls import path, path
from . import views

urlpatterns = [
    path('login', views.userLogin),

    path('usuario/<int:id>', views.usuarioHandler),
    path('usuario', views.usuarioHandler),

    path('paciente/<int:id>', views.pacienteHandler),
    path('paciente', views.pacienteHandler),

    path('funcionario/<int:id>', views.funcionarioHandler),
    path('funcionario', views.funcionarioHandler),

    path('especialidade', views.especialidadeHandler),

    path('visita/<int:id>', views.visitaHandler),
    path('visita', views.visitaHandler),

    path('alta-visita/<int:id>', views.altaVisita),

    path('quarto/<int:id>', views.quartoHandler),
    path('quarto', views.quartoHandler),

    path('andar', views.andarHandler),
    path('status-quarto', views.statusQuartoHandler),

    path('fila/<int:id>', views.filaHandler),
    path('fila', views.filaHandler),
]

