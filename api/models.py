from django.db import models
from django.utils import timezone


class Andar(models.Model):
    id = models.IntegerField(primary_key=True)

    class Meta:
        db_table = 'andar'


class Diagnostico(models.Model):
    data_diagnostico = models.DateTimeField()
    visita = models.ForeignKey('Visita', models.DO_NOTHING, db_column='id_visita', null=True)
    contagioso = models.BooleanField()
    descricao = models.TextField()

    class Meta:
        db_table = 'diagnostico'


class MedicoFormacao(models.Model):
    medico = models.OneToOneField('Medico', models.DO_NOTHING, db_column='medico_id', primary_key=True)
    especialidade = models.OneToOneField('Especialidade', models.DO_NOTHING, db_column='especialidade_id',
                                         related_name='especialidade')

    class Meta:
        auto_created = True
        db_table = 'medico_formacao'
        unique_together = (('medico', 'especialidade'),)


class Especialidade(models.Model):
    nome = models.CharField(max_length=60)

    class Meta:
        db_table = 'especialidade'


class Medico(models.Model):
    quantidade_visitas = models.IntegerField(default=0)
    data_ultimo_atendimento = models.DateTimeField(blank=True, null=True)
    pessoa = models.OneToOneField('Pessoa', models.DO_NOTHING, db_column='id_pessoa', blank=True, null=True)
    crm = models.CharField(max_length=30, null=False, blank=False)
    formacao = models.ManyToManyField(Especialidade, through='MedicoFormacao')
    exercicio = models.OneToOneField(Especialidade, models.DO_NOTHING, db_column='id_especialidade_exercicio',
                                     related_name='exercicio', default=None)

    # visitas = models.ForeignKey('Visita', models.DO_NOTHING, related_name='medico_visitas', null=True)

    class Meta:
        db_table = 'medico'


class Atendente(models.Model):
    num_cracha = models.IntegerField(null=False)
    trabalha_final_de_semana = models.BooleanField(null=False, blank=False)
    pessoa = models.OneToOneField('Pessoa', models.DO_NOTHING, db_column='id_pessoa')

    class Meta:
        db_table = 'atendente'


class MovimentacaoPaciente(models.Model):
    id_visita = models.ForeignKey('Visita', models.DO_NOTHING, db_column='id_visita')
    data_movimentacao = models.DateTimeField()
    observacao = models.TextField(blank=True, null=True)
    id_status_movimentacao = models.ForeignKey('StatusPacienteQuarto', models.DO_NOTHING,
                                               db_column='id_status_movimentacao', blank=True, null=True)
    quarto = models.CharField(max_length=5, blank=True, null=True)

    class Meta:
        db_table = 'movimentacao_paciente'


class Paciente(models.Model):
    data_ultima_consulta = models.DateTimeField(blank=True, null=True)
    convenio = models.BooleanField(default=False)
    pessoa = models.OneToOneField('Pessoa', models.DO_NOTHING, db_column='id_pessoa')
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)
    deactivated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = 'paciente'

    def delete(self):
        self.deactivated_at = timezone.now() if self.deactivated_at is None else None
        self.save()


class Alta(models.Model):
    data_alta = models.DateTimeField()
    visita = models.OneToOneField('Visita', models.DO_NOTHING, db_column='id_visita')
    observacao = models.TextField(blank=True, null=True)

    class Meta:
        db_table = 'alta'


class Pessoa(models.Model):
    nome = models.CharField(max_length=70)
    data_nascimento = models.DateField()
    cpf = models.CharField(max_length=11, unique=True)
    fone = models.CharField(max_length=11)
    email = models.CharField(max_length=50, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    sexo = models.CharField(max_length=1)

    class Meta:
        db_table = 'pessoa'


class Quarto(models.Model):
    numero = models.CharField(max_length=5)
    andar = models.ForeignKey('Andar', models.DO_NOTHING, db_column='andar')
    capacidade = models.IntegerField()
    status = models.ForeignKey('StatusQuarto', models.DO_NOTHING, db_column='id_status')
    quarentena = models.BooleanField()

    class Meta:
        db_table = 'quarto'


class StatusPacienteQuarto(models.Model):
    nome = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        db_table = 'status_paciente_quarto'


class StatusQuarto(models.Model):
    nome = models.CharField(max_length=50)

    class Meta:
        db_table = 'status_quarto'


class Funcionario(models.Model):
    data_contratacao = models.DateField(null=False, blank=False)
    data_demissao = models.DateField(null=True, blank=True)
    tipo = models.CharField(max_length=50)
    salario = models.FloatField(null=True)
    medico = models.OneToOneField('Medico', models.DO_NOTHING, db_column='id_medico', null=True, blank=True)
    atendente = models.OneToOneField('Atendente', models.DO_NOTHING, db_column='id_atendente', null=True, blank=True)

    class Meta:
        db_table = 'funcionario'


class Usuario(models.Model):
    last_login = models.DateTimeField(blank=True, null=True)
    username = models.CharField(max_length=20, db_column='usuario', unique=True)
    senha = models.CharField(max_length=255)
    funcionario = models.OneToOneField('Funcionario', models.DO_NOTHING, db_column='id_funcionario')
    ativo = models.BooleanField(default=True)

    class Meta:
        db_table = 'usuario'


class StatusVisita(models.Model):
    nome = models.CharField(max_length=50)

    class Meta:
        db_table = 'status_visita'


class Visita(models.Model):
    data_visita = models.DateTimeField()
    paciente = models.ForeignKey(Paciente, models.DO_NOTHING, db_column='id_paciente')
    medico = models.ForeignKey(Medico, models.DO_NOTHING, db_column='id_medico', related_name='visitas')
    sintomas = models.TextField(blank=True, null=True)
    pressao = models.CharField(max_length=10, blank=True, null=True)
    temperatura = models.FloatField(blank=True, null=True)
    descricao_estado = models.TextField(blank=True, null=True)
    status_visita = models.ForeignKey(StatusVisita, models.DO_NOTHING, db_column='id_status_visita')
    ordem_atendimento = models.IntegerField(default=0, null=False, blank=False)

    class Meta:
        db_table = 'visita'
        ordering = ['medico', 'ordem_atendimento', 'paciente']
