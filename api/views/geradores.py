# from faker import Faker
# from faker.providers import date_time, ssn, phone_number, internet
# from api.models import Paciente, Pessoa
# from api.serializers import PacienteSerializer
#
# from django.utils.timezone import now
#
# import re, random
#
# from rest_framework.response import Response
# from rest_framework import status
# from rest_framework.decorators import api_view
#
# @api_view(['POST'])
# def geradorPacientes():
#     fake = Faker('pt_BR')
#     fake.add_provider(date_time)
#     fake.add_provider(ssn)
#     fake.add_provider(phone_number)
#     fake.add_provider(internet)
#
#     for i in range(1, 2):
#         novo = Paciente()
#         novo.pessoa.nome = fake.name()
#         novo.pessoa.data_nascimento = fake.past_date()
#         novo.pessoa.cpf = re.sub("\D", "", fake.cpf())
#         novo.pessoa.fone = re.sub("\D", "", fake.phone_number())
#         novo.pessoa.email = fake.email()
#         novo.pessoa.created_at = now()
#         novo.pessoa.sexo = random.choice(['M', 'F'])
#
#         novo.convenio = random.choice([True, False])
#         novo.created_at = now()
#         novo.save()
#
#     return Response(status=status.HTTP_201_CREATED)
