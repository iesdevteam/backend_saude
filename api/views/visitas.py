from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import Visita, StatusVisita, Alta
from api.serializers import VisitaSerializer
from rest_framework import status

import datetime


@api_view(['GET', 'POST', 'PUT'])
def visitaHandler(request, id=None):
    how = request.method
    if how == 'GET':
        if id is not None:
            query = Visita.objects.get(id=id)
            serial = VisitaSerializer(query, many=False)
            return Response(serial.data)

        if id is None:
            params = request.GET
            query = Visita.objects.all().order_by('data_visita').order_by('data_visita').order_by('ordem_atendimento')

            if request.user_info.funcionario.tipo == 'medico':
                query = query.filter(medico=request.user_info.funcionario.medico)

            if 'limit' in params:
                query = query[:int(params['limit'])]

            if 'paciente' in params:
                query = query.filter(paciente__pessoa__nome__icontains=params['paciente'])

            if 'medico' in params and params['medico'] != 0:
                query = query.filter(medico=params['medico'])

            if 'data_visita' in params and params['data_visita']:
                query = query.filter(data_visita__date=params['data_visita']).distinct()

            serial = VisitaSerializer(query, many=True)
            return Response(serial.data)

    if how == 'POST':
        data = request.data

        visita_em_aberto = Visita.objects.all().filter(paciente__pessoa__cpf=data['paciente']['pessoa']['cpf']).filter(
            status_visita__id=1)

        if visita_em_aberto.count() > 0:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'message': 'Paciente já com visita em aberto!'})
        else:
            data['status_visita'] = {"id": 1, "nome": "Na fila de Atendimento"}
            novo = VisitaSerializer(data=data)
            if novo.is_valid(raise_exception=True):
                novo.save()
                return Response(status=status.HTTP_201_CREATED)

    if how == 'PUT':
        data = request.data
        atual = Visita.objects.get(id=id)
        editado = VisitaSerializer(atual, data=data)

        if editado.is_valid(raise_exception=True):
            editado.save()
            return Response(status=status.HTTP_201_CREATED)


@api_view(['PUT', ])
def altaVisita(request, id):
    try:
        data = request.data
        visita = Visita.objects.get(id=id)
        visita.status_visita = StatusVisita.objects.get(id=3)
        visita.save()

        alta = Alta()
        alta.visita = visita
        alta.observacao = data['observacao']
        alta.data_alta = data['data_alta']
        alta.save()

        return Response(status=status.HTTP_201_CREATED)
    except ValueError:
        return Response(status=status.HTTP_400_BAD_REQUEST, data=ValueError)
