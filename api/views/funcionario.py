from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Medico, Usuario
from api.serializers import UsuarioSerializer

from passlib.hash import pbkdf2_sha256


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def funcionarioHandler(request, id=None):
    how = request.method

    if how == 'GET':
        params = request.GET
        if id is None:
            query = Usuario.objects.all().order_by('id')

            if 'tipo' in params:
                query = query.filter(funcionario__tipo=params['tipo'])

            if request.user_info.funcionario.tipo == 'medico':
                print('medico')
                print(request.user_info.funcionario.medico)
                query = query.filter(funcionario__medico=request.user_info.funcionario.medico)

            serial = UsuarioSerializer(query, many=True)
            return Response(serial.data)
        else:
            query = Usuario.objects.get(id=id)
            serial = UsuarioSerializer(query, many=False)
            return Response(serial.data)

    if how == 'POST':
        data = request.data
        senha = pbkdf2_sha256.hash((data['senha']))
        data['senha'] = senha
        novoMedico = UsuarioSerializer(data=data)
        if novoMedico.is_valid(raise_exception=True):
            novoMedico.save()
            return Response(status=status.HTTP_201_CREATED)

    if how == 'PUT':
        data = request.data
        # senha = pbkdf2_sha256.hash((data['senha']))
        # data['senha'] = senha
        atual = Usuario.objects.get(id=id)
        editado = UsuarioSerializer(atual, data=data)
        if editado.is_valid(raise_exception=True):
            editado.save()
            return Response(status=status.HTTP_201_CREATED)

    if how == 'DELETE':
        atual = Usuario.objects.get(id=id)
        atual.ativo = not atual.ativo
        atual.save()
        return Response(status=status.HTTP_200_OK)
