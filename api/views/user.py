from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Usuario
from api.serializers import UsuarioSerializer

from passlib.hash import pbkdf2_sha256
from rest_framework_jwt.settings import api_settings


@api_view(['POST'])
def userLogin(request):
    data = request.data
    user = Usuario.objects.filter(username__exact=data['usuario']).first()
    if user:
        if user.ativo is True:
            json = UsuarioSerializer(user, many=False)
            if pbkdf2_sha256.verify(data['senha'], user.senha):
                jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
                jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

                payload = jwt_payload_handler(user)
                token = jwt_encode_handler(payload)
                return Response({'auth': True, 'token': token, 'user': json.data})
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST, data='Senha incorreta!')
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST, data={'error': 'Usuário está inativo!'})
    else:
        return Response(status=status.HTTP_400_BAD_REQUEST, data='Usuário não existe!')
