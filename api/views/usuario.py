from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Usuario
from api.serializers import UsuarioSerializer

from passlib.hash import pbkdf2_sha256


@api_view(['POST', 'GET'])
def usuarioHandler(request, id=None):
    how = request.method

    if how == 'GET':
        query = Usuario.objects.get(id=id)
        ser = UsuarioSerializer(query, many=False)
        return Response(ser.data)
    if how == 'POST':
        data = request.data
        senha = pbkdf2_sha256.hash((data['senha']))
        data['senha'] = senha
        user = UsuarioSerializer(data=data)
        if user.is_valid(raise_exception=True):
            user.save()
            return Response(status=status.HTTP_201_CREATED, data={'message': "Usuario criado!"})
        else:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR, data={'message': "DEU RIUM!"})
