from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.validators import ValidationError

from api.models import Paciente
from api.serializers import PacienteSerializer


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def pacienteHandler(request, id=None):
    how = request.method

    if how == 'GET':  # RECUPERA APENAS UM OBJECTO
        if id is not None:
            query = Paciente.objects.get(id=id)
            serial = PacienteSerializer(query)
            return Response(serial.data)
        else:  # RETORNA A LISTA DE TODOS OS PACIENTES
            query = Paciente.objects.all()
            if request.GET and request.GET['search']:
                query = query.filter(pessoa__nome__startswith=request.GET['search'])

            serial = PacienteSerializer(query, many=True)
            return Response(serial.data)

    if how == 'POST':
        data = request.data
        paciente = PacienteSerializer(data=data)
        if paciente.is_valid(raise_exception=True):
            paciente.save()
            return Response(status=status.HTTP_201_CREATED)

    if how == 'PUT':
        data = request.data
        pacienteAtual = Paciente.objects.get(id=id)
        pacienteEditado = PacienteSerializer(pacienteAtual, data=data)
        if pacienteEditado.is_valid(raise_exception=True):
            pacienteEditado.save()
            return Response(status=status.HTTP_201_CREATED)

    if how == 'DELETE':
        adeletar = Paciente.objects.get(id=id)
        adeletar.delete()
        return Response(status=status.HTTP_200_OK)
