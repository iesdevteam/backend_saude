from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from api.models import Especialidade
from api.serializers import EspecialidadeSerializer


@api_view(['GET'])
def especialidadeHandler(request):
    query = Especialidade.objects.all().order_by('nome')
    serial = EspecialidadeSerializer(query, many=True)
    return Response(serial.data)
