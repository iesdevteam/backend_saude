from api.models import Medico, Visita
from rest_framework.decorators import api_view
from rest_framework import status
from rest_framework.response import Response
from api.serializers import MedicoSerializer, VisitaSerializer

from django.utils import timezone


@api_view(['GET', 'PUT'])
def filaHandler(request, id=None):
    how = request.method

    if how == 'GET':
        params = request.GET
        fila = []
        medicos = Medico.objects.all()

        if request.user_info.funcionario.tipo == 'medico':
            medicos = medicos.filter(id=request.user_info.funcionario.medico.id)

        for m in medicos:
            temp = MedicoSerializer(m, many=False).data
            temp['visitas'] = Visita.objects.all().filter(medico=m).filter(data_visita__startswith=params['data'])
            temp['visitas'] = VisitaSerializer(temp['visitas'], many=True).data

            fila.append(temp)

        return Response(fila)


    if how == 'PUT':
        data = request.data
        for v in data:
            tmp = Visita.objects.get(id=v['id'])
            tmp.ordem_atendimento = v['ordem']
            tmp.save()
        return Response(status=status.HTTP_200_OK)
