from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import Quarto, Andar, StatusQuarto
from api.serializers import QuartoSerializer, AndarSerializer, StatusQuartoSerializer
from rest_framework import status


@api_view(['GET', 'POST', 'PUT', 'DELETE'])
def quartoHandler(request, id=None):
    how = request.method
    if how == 'GET':
        filters = request.GET
        query = Quarto.objects.all().order_by('andar', 'numero')
        serial = QuartoSerializer(query, many=True)
        return Response(serial.data)

    if how == 'POST':
        data = request.data

        novo = QuartoSerializer(data=data)
        if novo.is_valid(raise_exception=True):
            novo.save()
            return Response(status=status.HTTP_201_CREATED)

        return Response(data)

    if how == 'PUT':
        data = request.data
        atual = Quarto.objects.get(id=data['id'])

        novo = QuartoSerializer(atual, data=data)
        if novo.is_valid(raise_exception=True):
            novo.save()
            return Response(status=status.HTTP_201_CREATED)


@api_view(['GET'])
def andarHandler(request):
    query = Andar.objects.all()
    serial = AndarSerializer(query, many=True)
    return Response(serial.data)


@api_view(['GET'])
def statusQuartoHandler(request):
    query = StatusQuarto.objects.all()
    serial = StatusQuartoSerializer(query, many=True)
    return Response(serial.data)
