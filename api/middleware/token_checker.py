from django.http import HttpResponse
from rest_framework_jwt.settings import api_settings
from api.models import Usuario
from django.conf import settings

class JWTChecker:
    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        if 'login' not in request.get_full_path() and getattr(settings, "IGNORE_AUTH", False) == False:
            if request.META.get('HTTP_AUTHORIZATION') is not None:
                try:
                    jwt_decode_handler = api_settings.JWT_DECODE_HANDLER
                    token = request.META.get('HTTP_AUTHORIZATION').split(' ', 1)[1]
                    user_data = jwt_decode_handler(token)
                    request.user_info = Usuario.objects.get(id=user_data['user_id'])
                    response = self.get_response(request)
                    return response
                except:
                    return HttpResponse('Unauthorized', status=401)
            else:
                return HttpResponse('Unauthorized', status=401)
        else:
            response = self.get_response(request)
            return response
