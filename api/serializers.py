from rest_framework import serializers
from drf_writable_nested import WritableNestedModelSerializer, UniqueFieldsMixin, NestedUpdateMixin
from .models import *


# PESSOA

class PessoaSerializer(UniqueFieldsMixin, serializers.ModelSerializer):
    class Meta:
        model = Pessoa
        fields = (
            'id', 'nome', 'data_nascimento', 'cpf', 'sexo', 'fone', 'email', 'created_at', 'deleted_at', 'updated_at')


# PACIENTE

class PacienteSerializer(WritableNestedModelSerializer, NestedUpdateMixin):
    pessoa = PessoaSerializer()

    class Meta:
        # depth = 1
        model = Paciente
        fields = ('id', 'data_ultima_consulta', 'convenio', 'pessoa', 'created_at', 'deactivated_at')


# ESPECIALIDADE

class EspecialidadeSerializer(WritableNestedModelSerializer):
    class Meta:
        model = Especialidade
        depth = 1
        fields = ('id', 'nome')


# FORMACAO MEDICO

class MedicoFormacaoSerializer(serializers.ModelSerializer):
    especialidade = EspecialidadeSerializer(many=True)

    class Meta:
        model = MedicoFormacao
        fields = ('especialidade',)


# STATUS VISITA

class StatusVisitaSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusVisita
        fields = ('id', 'nome')


class VisitaMedicoSerializer(serializers.ModelSerializer):
    paciente = PacienteSerializer(many=False, required=True, read_only=False)
    # medico = MedicoSerializer(many=False, required=True, read_only=False)
    status_visita = StatusVisitaSerializer(many=False, read_only=False, allow_null=True)

    class Meta:
        model = Visita
        fields = ('id', 'paciente', 'data_visita', 'sintomas', 'pressao', 'temperatura', 'descricao_estado',
                  'status_visita', 'ordem_atendimento')


# MEDICO
class MedicoSerializer(WritableNestedModelSerializer, NestedUpdateMixin):
    pessoa = PessoaSerializer(many=False, read_only=False)
    formacao = EspecialidadeSerializer(many=True, read_only=False, required=False)
    exercicio = EspecialidadeSerializer(many=False, read_only=False, required=False)
    visitas = VisitaMedicoSerializer(many=True, read_only=True)

    class Meta:
        model = Medico
        depth = 1
        fields = (
        'id', 'quantidade_visitas', 'data_ultimo_atendimento', 'pessoa', 'crm', 'formacao', 'exercicio', 'visitas')


# ATENDENTE

class AtendenteSerializer(WritableNestedModelSerializer, NestedUpdateMixin):
    pessoa = PessoaSerializer(many=False, read_only=False)

    class Meta:
        model = Atendente
        # depth = 1
        fields = ('id', 'num_cracha', 'trabalha_final_de_semana', 'pessoa')


# FUNCIONARIO

class FuncionarioSerializer(WritableNestedModelSerializer, NestedUpdateMixin):
    medico = MedicoSerializer(many=False, read_only=False, required=False, allow_null=True)
    atendente = AtendenteSerializer(many=False, read_only=False, required=False, allow_null=True)

    class Meta:
        model = Funcionario
        depth = 1
        fields = ('id', 'data_contratacao', 'data_demissao', 'tipo', 'salario', 'medico', 'atendente')


# USUARIO

class UsuarioSerializer(WritableNestedModelSerializer, NestedUpdateMixin):
    funcionario = FuncionarioSerializer(many=False, read_only=False)
    senha = serializers.CharField(required=False)

    class Meta:
        model = Usuario
        depth = 1
        fields = ('id', 'last_login', 'username', 'senha', 'funcionario', 'ativo')

    def to_representation(self, instance):
        rep = super(UsuarioSerializer, self).to_representation(instance)
        rep.pop('senha', None)
        return rep


# VISITA

class VisitaSerializer(WritableNestedModelSerializer):
    paciente = PacienteSerializer(many=False, required=True, read_only=False)
    medico = MedicoSerializer(many=False, required=True, read_only=False)
    status_visita = StatusVisitaSerializer(many=False, read_only=False, allow_null=True)

    class Meta:
        model = Visita
        fields = ('id', 'paciente', 'medico', 'data_visita', 'sintomas', 'pressao', 'temperatura', 'descricao_estado',
                  'status_visita', 'ordem_atendimento')


# SERIALIZERS DE QUARTOS

class StatusQuartoSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatusQuarto
        fields = ('id', 'nome')


class AndarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Andar
        fields = ('id',)


class QuartoSerializer(serializers.ModelSerializer):
    quarentena = serializers.BooleanField(required=False, default=0)

    class Meta:
        model = Quarto
        fields = '__all__'
